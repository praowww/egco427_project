import Vue from 'vue'
import Router from 'vue-router'
import profile from '@/components/profile'
import signin from '@/components/signin'
import signup from '@/components/signup'
import home from '@/components/home'
import region from '@/components/region'
import detail from '@/components/detail'
import SuiVue from 'semantic-ui-vue';
Vue.use(Router)
Vue.use(SuiVue)
let router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/signin'
    },
    {
      path: '*',
      redirect: '/signin'
    },
    {
      path: '/signin',
      name: 'signin',
      component: signin
    },
    {
      path: '/signup',
      name: 'signup',
      component: signup
    },
    {
      path: '/home/:username',
      name: 'home',
      component: home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/profile/:username',
      name: 'profile',
      component: profile,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/region/:regionname/:username',
      name: 'region',
      component: region,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/detail/:placename/:username',
      name: 'detail',
      component: detail,
      meta:{
        requiresAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  if (requiresAuth && !currentUser) {
    next('signin')
  } else if (!requiresAuth && currentUser) {
    next('home','signup')
  } else {
    next()
  }
})

export default router