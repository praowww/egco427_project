// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'
import SuiVue from 'semantic-ui-vue'
import BootstrapVue from 'bootstrap-vue'
import * as VueGoogleMaps from "vue2-google-maps";
//import date from './date-and-time'

Vue.use(SuiVue)

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyCE8JnG7KAjG2I475jN4Ciw6D7-6BXMAW4",
    libraries: "places" // necessary for places input
  }
});

Vue.use(BootstrapVue)
// Initialize Firebase

// var config = {
//   apiKey: "AIzaSyBMal8TBKbYyJcy8uYRooSXHTpxNsBQBZU",
//   authDomain: "egco427-practice-3ab58.firebaseapp.com",
//   databaseURL: "https://egco427-practice-3ab58.firebaseio.com",
//   projectId: "egco427-practice-3ab58",
//   storageBucket: "egco427-practice-3ab58.appspot.com",
//   messagingSenderId: "135472559888"
// };
var config = {
  apiKey: "AIzaSyBtbAqLkTKAs3j3kDFuiSYU5Dw7npEJQQA",
  authDomain: "project-9d595.firebaseapp.com",
  databaseURL: "https://project-9d595.firebaseio.com",
  projectId: "project-9d595",
  storageBucket: "",
  messagingSenderId: "509190087144"
};

firebase.initializeApp(config);

let app
window.firebase = firebase //can use firebase in overall of this project

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

firebase.auth().onAuthStateChanged((user) => {
  if(!app){
    app = new Vue({
      el: '#app',
      router,
      components: { App },
      template: '<App/>'
    })
  }
})
